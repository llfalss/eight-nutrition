<?php
//Template name: Atletas
get_header();
get_template_part("template/global","banner");
get_template_part("template/global","about");
get_template_part("template/global","whois");
get_template_part("template/global","benefits");
get_template_part("template/global","testimony");
get_template_part("template/global","recommendation");
get_template_part("template/global","products");
get_template_part("template/global","newsletter");  
get_footer();