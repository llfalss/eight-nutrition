jQuery(document).ready(function($) {
  $('.ht-mask__money').mask('#.##0,00', {reverse: true, placeholder : "0,00"});
  $('.ht-mask__date').mask("00/00/0000", {placeholder: "__/__/____"});
  $('.ht-mask__cnpj').mask('00.000.000/0000-00');
  $('.ht-mask__cpf').mask('000.000.000-00');
  $('.ht-mask__cep').mask("00000-000", {placeholder: "_____-___"});
  var optionsPhone = {onKeyPress : function(val){}}
  var SPMaskBehavior = function (val) { return val.replace(/\D/g, '').length === 11 ? '(00) 0 0000-0000' : '(00) 0000-00009'; }, spOptions = { placeholder: "(XX) X XXXX-XXXX", onKeyPress: function(val, e, field, options) { field.mask(SPMaskBehavior.apply({}, arguments), options); } };
  $('.ht-mask__cel').mask(SPMaskBehavior, spOptions);

  $(".service__gallery").lightGallery({
    thumbnail:false
  });

  $(".ht-header__control--js").on("click", function(){
    $("body").toggleClass("ht-header__nav--active");
  });

  $(".home-slider--js").slick({
    prevArrow : "<div class='home-slider__nav home-slider__nav--prev'><i class='fas fa-chevron-left'></i></div>",
    nextArrow : "<div class='home-slider__nav home-slider__nav--next'><i class='fas fa-chevron-right'></i></div>",
  });

  $(".testimony__items--js").slick({
    prevArrow: "<span class='testimony__control testimony__control--prev'>Prev</span>",
    nextArrow: "<span class='testimony__control testimony__control--next'>Next</span>",
  })

  $(".service__item--js").on("click", function(e){
    e.preventDefault();
    var item = $(this).attr("data-service");
    $(item).css({
      opacity : 1,
      transform : "translateX(0)",
    });
    $("body").css({
      overflow : "hidden",
    });
    // console.log($(item));
  });

  $(".service__close--js").on("click", function(e){
    e.preventDefault();
    var item = $(this).attr("data-close");
    $(item).css({
      opacity : 0,
      transform : "translateX(-150%)",
    });
    $("body").css({
      overflow : "auto",
    });
  });
  //console.log("teste");
  $(window).scroll(function(){
    var credit = $(".footer__items").offset().top;
    var pageTop = $(window).scrollTop();
    var pageBottom = pageTop + $(window).height();

    if(pageBottom >= credit){
      $("body").addClass("ht-wpp__hide");
    }else{
      $("body").removeClass("ht-wpp__hide");
    }

    //console.log("Credito: "+ credit +"\nTela: "+ pageBottom);
  });
   $(".ht-mobile__subitem").hide();
  $(".ht-nav__mobile--js").on("click", function(){
    $("html").toggleClass("ht-nav__mobile--active");
  })
  $(".ht-nav__mobile--dropdown").on("click", function(e){
    e.preventDefault();
    $(this).find(".ht-mobile__subitem").slideToggle();
    $(this).toggleClass("ht-mobile__subitem--active");
  });
  $(".home-banner--js").slick({
    prevArrow : null,
    nextArrow : null,
    dots : true,
    autoplay : true,
    pauseOnHover : true,
  });
  $('.parceiros__list--home').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerPadding: "60px",
          dots : true,
        }
      },
    ]
  });

  $(".modal__wrapper").hide();
  // $(".modal__wrapper").on("click",function(){
  //   $(this).fadeOut();
  //   $("body").css({
  //     overflow : "auto",
  //   });
  // })
  $(".modal__show").on("click", function(e){
    e.preventDefault();
    var modal = "."+ $(this).attr("data-id");
    $(modal).fadeIn();
    $("body").css({
      overflow : "hidden",
    });
  });
  $(".modal__close").on("click", function(e){
    e.preventDefault();
    var modal = "."+ $(this).attr("data-id");
    $(modal).fadeOut();
    $("body").css({
      overflow : "auto",
    });
  });
  $(".parceiro__combo").on("change",function(){
    var tipo = $(this).val();
    $(".parceiro__item").hide();
    if(tipo == "1"){
      $(".parceiro__item").show();
    }else{
      $("."+ tipo).show();
    }
  });

  $(".galeria__search--field").on("input", function(){
    var valor = $(this).val().trim().toLowerCase();
    console.log(valor);
    $(".galeria__item").each(function(){
      if(valor.length < 1){
        $(this).show();
      }else{
        $(this).toggle($(this).filter("[data-title*='"+ valor +"']").length > 0);
      }
    })
  });

  $(".galeria__content").lightGallery();

  $(".depoimentos--video-js").slick({
    nextArrow : "<div class=\"depoimento__arrow depoimento__arrow--next\">Próximo</div>",
    prevArrow : "<div class=\"depoimento__arrow depoimento__arrow--prev\">Anterior</div>",
  });

  $(".depoimentos--texto-js").slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    nextArrow : "<div class=\"depoimento__arrow depoimento__arrow--texto depoimento__arrow--next\">Próximo</div>",
    prevArrow : "<div class=\"depoimento__arrow depoimento__arrow--texto depoimento__arrow--prev\">Anterior</div>",
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
    ]
  });

  $(".image__wrapper").lightGallery({
    thumbnail : false,
  });
  if ($(window).width() < 1000){
    $(".blog__list--home").slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow : null ,
      prevArrow : null ,
      dots: true,
    });
  }
  $(".products").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: true,
    arrows: false,
  })
  $(".testimonys").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    nextArrow: '.nextArrow',
    prevArrow: '.prevArrow',
  })

});
