<?

    $themeName = ht_get_theme_name();

?>
<div class="ht__benefits" style="background: url(<? print ht_get_theme_image("image/$themeName/bg_benefits.png") ?>);  background-size: cover; ">
    <div class="header">
        <img src="<? print ht_get_theme_image("image/$themeName/icon_benefits.svg") ?>" alt="" srcset="">

        <h1>Benefícios do <span>NEURO ACTIVE</span></h1>
    </div>
    <div class="benefits_1">
        <ul>
        <?
            if( have_rows('ht_benefits') ):
                while( have_rows('ht_benefits') ) : the_row();
                $sub_value = get_sub_field('ht_benefit-1');
        ?>   
            <li>
                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0)">
                        <path
                            d="M28.6901 1.95274C27.3363 1.2238 25.8263 2.62962 24.9412 3.46269C22.9107 5.44126 21.1924 7.73222 19.2659 9.81491C17.1311 12.1059 15.1525 14.3968 12.9657 16.6358C11.7161 17.8854 10.3623 19.2392 9.52925 20.8012C7.65483 18.9788 6.04074 17.0002 3.95804 15.3862C2.44809 14.2407 -0.0511446 13.4076 0.000922822 16.1672C0.105058 19.76 3.28117 23.6129 5.6242 26.06C6.61348 27.1014 7.91517 28.1948 9.42512 28.2469C11.2475 28.351 13.1219 26.1642 14.2153 24.9666C16.1419 22.8839 17.7039 20.5408 19.4741 18.4061C21.7651 15.5945 24.1081 12.8348 26.347 9.97112C27.7528 8.20083 32.1786 3.82708 28.6901 1.95274ZM2.2918 15.959C2.23974 15.959 2.18767 15.959 2.08353 16.0109C1.87526 15.959 1.71906 15.9068 1.51079 15.8027C1.66699 15.6985 1.92733 15.7506 2.2918 15.959Z"
                            fill="#97C434" />
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="30" height="30" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
               <? print $sub_value ?>
            </li>
        <?
                endwhile;
            endif;
        ?>
        </ul>
    </div>
    <div class="benefits_2">
        <ul>
            <?
                if( have_rows('ht_benefits') ):
                    while( have_rows('ht_benefits') ) : the_row();
                    $sub_value = get_sub_field('ht_benefit-2');
            ?>   
                <li>
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0)">
                            <path
                                d="M28.6901 1.95274C27.3363 1.2238 25.8263 2.62962 24.9412 3.46269C22.9107 5.44126 21.1924 7.73222 19.2659 9.81491C17.1311 12.1059 15.1525 14.3968 12.9657 16.6358C11.7161 17.8854 10.3623 19.2392 9.52925 20.8012C7.65483 18.9788 6.04074 17.0002 3.95804 15.3862C2.44809 14.2407 -0.0511446 13.4076 0.000922822 16.1672C0.105058 19.76 3.28117 23.6129 5.6242 26.06C6.61348 27.1014 7.91517 28.1948 9.42512 28.2469C11.2475 28.351 13.1219 26.1642 14.2153 24.9666C16.1419 22.8839 17.7039 20.5408 19.4741 18.4061C21.7651 15.5945 24.1081 12.8348 26.347 9.97112C27.7528 8.20083 32.1786 3.82708 28.6901 1.95274ZM2.2918 15.959C2.23974 15.959 2.18767 15.959 2.08353 16.0109C1.87526 15.959 1.71906 15.9068 1.51079 15.8027C1.66699 15.6985 1.92733 15.7506 2.2918 15.959Z"
                                fill="#97C434" />
                        </g>
                        <defs>
                            <clipPath id="clip0">
                                <rect width="30" height="30" fill="white" />
                            </clipPath>
                        </defs>
                    </svg>
                <? print $sub_value ?>
                </li>
            <?
                    endwhile;
                endif;
            ?>
        </ul>
    </div> -->
    <a class="products-link" href="#products">
        Compre Agora Seu Neuro Active
    </a>
</div>