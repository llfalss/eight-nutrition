<?
    $whoisGroup = get_field('ht_whois-group');
    $whoisPhoto = $whoisGroup['ht_whois-photo'];
    $whoisText = $whoisGroup['ht_whois-text'];

?>
<div class="ht__whois">
    <div class="pp">
        <img src="<? print $whoisPhoto ?>" alt="pp">
     
    </div>
    <div>
        <h1>Quem criou o <br><span>Neuro Active?</span></h1>
        <p>
            <? print $whoisText?>
        </p>
    </div>
</div>