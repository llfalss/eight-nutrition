<?

    $themeName = ht_get_theme_name();
    // $posts = get_posts( );
    // var_dump($posts);

    $args = array(  
        'post_type' => 'produto',
        'post_status' => 'publish',
        'orderby' => 'post_date', 
        'order' => 'ASC', 
    );
    $loop = new WP_Query( $args ); 
         
?>
<div class="ht__products" id="products" style="background: url(<? print ht_get_theme_image("image/$themeName/bg_products.png") ?>); background-size: cover; ">
    <h1>Adquira já e alcance <br> <span>Melhores resultados</span></h1>
    <div class="products">
    <?
        while ( $loop->have_posts() ) : $loop->the_post(); 
    ?>
    <div class="product">
            <h1><? print the_title() ?></h1>
            <p class="price" style="color: <? print get_field('ht_price-color') ?> !important">R$<? print str_replace(".", ",", get_field('ht_product-price')) ?></p>
            <p class="about-product">
            <? print get_field('ht_product-detail') ?>
            </p>
            
            <a style="background-color: <? print get_field('ht_price-color') ?> !important" href="<? print get_field('link_para_compra')?>">Compre agora</a>
        </div>    
    <?
        endwhile;

        wp_reset_postdata();
    ?>
    </div>
</div>