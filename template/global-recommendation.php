<?

    $themeName = ht_get_theme_name();

?>
<div class="ht__recommendations" style="background: url(<? print ht_get_theme_image("image/$themeName/bg_recommendations.png") ?>); background-size: cover; ">
    
    <div>
        <div class="sim">
            <h1><span>Para quem é <br></span> esse produto?</h1>
                <ul>
                    <?
                        if( have_rows('ht_to-whois') ):
                            while( have_rows('ht_to-whois') ) : the_row();
                                $sub_value = get_sub_field('ht_to-whois-item');
                    ?>
                                <li>
                                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0)">
                                            <path
                                                d="M28.6901 1.9525C27.3363 1.22355 25.8263 2.62937 24.9412 3.46245C22.9107 5.44101 21.1924 7.73198 19.2659 9.81467C17.1311 12.1056 15.1525 14.3966 12.9657 16.6356C11.7161 17.8852 10.3623 19.239 9.52925 20.801C7.65483 18.9785 6.04074 17 3.95804 15.386C2.44809 14.2405 -0.0511446 13.4074 0.000922822 16.167C0.105058 19.7597 3.28117 23.6127 5.6242 26.0598C6.61348 27.1011 7.91517 28.1945 9.42512 28.2466C11.2475 28.3507 13.1219 26.1639 14.2153 24.9664C16.1419 22.8837 17.7039 20.5406 19.4741 18.4059C21.7651 15.5942 24.1081 12.8346 26.347 9.97087C27.7528 8.20058 32.1786 3.82684 28.6901 1.9525ZM2.2918 15.9587C2.23974 15.9587 2.18767 15.9587 2.08353 16.0107C1.87526 15.9587 1.71906 15.9066 1.51079 15.8024C1.66699 15.6983 1.92733 15.7504 2.2918 15.9587Z"
                                                fill="#97C434" />
                                        </g>
                                        <defs>
                                            <clipPath id="clip0">
                                                <rect width="30" height="30" fill="white" />
                                            </clipPath>
                                        </defs>
                                    </svg>
                                    <p>
                                        <? print $sub_value ?>
                                    </p>
                                </li>
                            <?
                        endwhile;
                        else :
                        endif;
                ?>
                </ul>
        </div>
        <span class="linha"><!-- linha --> </span>
        <div class="nao">
            <h1><span>Para quem Não é <br></span> esse produto?</h1>
               
                <ul>
                    <?
                        if( have_rows('ht_to-whois-not') ):
                            while( have_rows('ht_to-whois-not') ) : the_row();
                                $sub_value = get_sub_field('ht_to-whois-item');
                    ?>
                                <li>
                                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M28.9861 5.90897L19.8963 14.9998L28.9861 24.0902C30.3382 25.4428 30.3382 27.6338 28.9861 28.9864C28.3106 29.662 27.4249 30 26.5396 30C25.6528 30 24.767 29.6625 24.092 28.9864L15.0001 19.895L5.90897 28.9863C5.23349 29.6619 4.34769 29.9999 3.46164 29.9999C2.57585 29.9999 1.69065 29.6624 1.01457 28.9863C-0.337502 27.6343 -0.337502 25.4432 1.01457 24.0901L10.1042 14.9997L1.01406 5.90897C-0.338019 4.55689 -0.338019 2.36536 1.01406 1.01328C2.36587 -0.33776 4.55612 -0.33776 5.90845 1.01328L15.0001 10.1041L24.091 1.01328C25.4436 -0.33776 27.6341 -0.33776 28.9856 1.01328C30.3382 2.36536 30.3382 4.55689 28.9861 5.90897Z"
                                        fill="#EB5757" />
                                </svg>
                                    <p>
                                        <? print $sub_value ?>
                                    </p>
                                </li>
                            <?
                        endwhile;
                        else :
                        endif;
                ?>
                </ul>
        </div>
    </div>
    <a class="products-link" href="#products">
        Compre Agora Seu Neuro Active
    </a>
</div>