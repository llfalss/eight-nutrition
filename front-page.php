<?php
get_header();
get_template_part("template/global","header");
get_template_part("template/home","slider");
get_template_part("template/home","about");
get_template_part("template/global","blog");
get_template_part("template/global","parceiros");
get_template_part("template/global","contact");
get_template_part("template/global","contact-info");
get_template_part("template/global","footer");
get_footer();
